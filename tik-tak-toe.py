"""
Nothing fancy here. A quick draft of a machine-learning-like algorithm, 
written from scratch. Trains an AI player on tik-tak-toe by letting two AIs to
play against each other (with some random behaviour), then plays a human.
"""

import os
import copy
from tqdm import tqdm
import Levenshtein as lev
import random as r


def check_field(field):
    """
    Performs a field check for end-game conditions
    """

    empty_found = False
    # Check horizontal
    for row in field:
        if row[0] == row[1] == row[2] != " ":
            return True
        elif " " in row:
            empty_found = True

    # Check vertical
    for col in range(0,3):
        if field[0][col] == field[1][col] == field[2][col] != " ":
            return True
        elif " " in (field[0][col], field[1][col], field[2][col]):
            empty_found = True

    # Check diagonal
    dia1 = (field[0][0], field[1][1], field[2][2])
    dia2 = (field[2][0], field[1][1], field[0][2])
    for dia in (dia1, dia2):
        if dia[0] == dia[1] == dia[2] != " ":
            return True
        elif " " in (dia[0], dia[1], dia[2]):
            empty_found = True

    # Check if there are empty boxes
    if not empty_found:
        return False

    # Game continues
    else:
        return None


def show_field(field_now, field_prev = None, show_grid = False):
    """Prints the field"""
    os.system("clear") # <----
    if show_grid:
        print("    1     2     3  ")
    for row, _ in enumerate(field_now):
        if show_grid:
            transform_indx = {0 : "a", 1 : "b", 2 : "c"}
            print(transform_indx[row], end = " ")
        for col, _ in enumerate(field_now[row]):
            if field_prev is not None \
                    and field_now[row][col] != field_prev[row][col]:
                print(f"[({field_now[row][col]})]", end = " ")
            else:
                print(f"[ {field_now[row][col]} ]", end = " ")
        print()


class Player:


    def _check_move(self, move, field):
        if field[move[0]][move[1]] != " ":
            return False
        else:
            return True

    def _make_move(self, field):

        def _check_user_input(cmd):
            if len(cmd) != 2:
                return False
            elif cmd[0].isalpha() is not True \
                    or cmd[0] not in ("a", "b", "c"):
                return False
            elif cmd[1].isnumeric() is not True \
                    or cmd[1] not in ("1", "2", "3"):
                return False
            return True

        def _get_user_input(field, og_error = ""):
            transform_alpha = {"a" : 0, "b" : 1, "c" : 2}
            transform_numbr = {"1" : 0, "2" : 1, "3" : 2}
            #os.system("clear")
            show_field(field, show_grid = True)
            cmd = input(f"{og_error} {self.sym} <-  make a move (e.g. 'a1'): ")
            while not _check_user_input(cmd):
                #os.system("clear")
                show_field(field, show_grid = True)
                cmd = input(f"{og_error} [Cmd ERROR!] {self.sym} <- make a move (e.g. 'a1'): ")
            cmd = [cmd[0], cmd[1]]
            cmd[0] = transform_alpha[cmd[0]] # transform letter to index
            cmd[1] = transform_numbr[cmd[1]] # transform number to index
            return cmd

        move = _get_user_input(field)
        while not self._check_move(move, field):
            og_error = "[Mov ERROR!]"
            move = _get_user_input(field, og_error = og_error)
        field[move[0]][move[1]] = self.sym
        return field


    def __str__(self):
        return self.id


    # In case you're playing manually
    def __init__(self):
        self.id = "HOOMANN"
        self.npc = False
        self.sym = None


class DumbAI(Player):

    def __init__(self):
        self.id = "DUMBASS"
        self.npc = True
        self.sym = None

    def _make_move(self, field): # override non-NPC function
        m = r.choice([0, 1, 2])
        n = r.choice([0, 1, 2])
        move = [m, n]
        while not self._check_move(move, field):
            m = r.choice([0, 1, 2])
            n = r.choice([0, 1, 2])
            move = [m, n]
        field[move[0]][move[1]] = self.sym
        return field


class SmartAI(Player):

    def __init__(self):
        self.id = "SMARTIE"
        self.npc = True
        self.sym = None


    def _one_shot(self, field):
        """
        Checks if it can one-shot-kill the opponent even if this isn't in
        the training data
        """

        # check rows
        for m, row in enumerate(field):
            syms = 0
            spcs = 0
            spc_at = None
            for n, sym in enumerate(row):
                if sym == self.sym:
                    syms += 1
                elif sym == " ":
                    spcs += 1
                    spc_at = n
            if spcs == 1 and syms == 2:
                return [m, spc_at]

        # check cols
        for n in range(3):
            syms = 0
            spcs = 0
            spc_at = None
            col = [field[0][n], field[1][n], field[2][n]]
            for m, sym in enumerate(col):
                if sym == self.sym:
                    syms += 1
                elif sym == " ":
                    spcs += 1
                    spc_at = m
            if spcs == 1 and syms == 2:
                return [spc_at, n]

        # check dias
        dia1 = (field[0][0], field[1][1], field[2][2])
        dia2 = (field[2][0], field[1][1], field[0][2])
        for i, dia in enumerate((dia1, dia2)):
            for ii, sym in enumerate(dia):
                if sym == self.sym:
                    syms += 1
                elif sym == " ":
                    spcs += 1
                    spc_at = ii
            if spcs == 1 and syms == 2:
                if spc_at == 1:
                    return [1, 1]
                elif spc_at == 0 and i == 0:
                    return [0, 0]
                elif spc_at == 0 and i == 1:
                    return [2, 0]
                elif spc_at == 2 and i == 0:
                    return [2, 2]
                elif spc_at == 2 and i == 1:
                    return [0, 2]
                else:
                    raise Exception("Unhandled case.")
        return False


    def _make_move(self, field): # override non-NPC function
        global training_data

        # One-shot-kill scenario? (sometimes no data on this)
        move = self._one_shot(field)
        if not move:
            pass
        else:
            next_field = field
            next_field[move[0]][move[1]] = self.sym    
            return next_field

        # Play from experience
        actual_word = _encode_field(field)
        max_win_prob = 0.0
        max_win_prob_word = ""
        for possible_word in training_data:
            lev_dist = lev.distance(actual_word, possible_word)
            if lev_dist == 1:
                for sym1, sym2 in zip(actual_word, possible_word):
                    if sym1 != sym2 and sym1 == " " and sym2 == self.sym:
                        prob = training_data[possible_word][self.sym]
                        if prob > max_win_prob:
                            max_win_prob = prob
                            max_win_prob_word = possible_word
        if max_win_prob_word != "":
            next_field = _decode_field(max_win_prob_word)

        # Play random
        else:
            m = r.choice([0, 1, 2])
            n = r.choice([0, 1, 2])
            move = [m, n]
            while not self._check_move(move, field):
                m = r.choice([0, 1, 2])
                n = r.choice([0, 1, 2])
                move = [m, n]
            next_field = field
            next_field[move[0]][move[1]] = self.sym

        return next_field


def _encode_field(field):
    word = []
    [[word.append(sym) for sym in row] for row in field]
    word = "".join(word)
    return word


def _decode_field(word):
    field = [[0,0,0],[0,0,0],[0,0,0]]
    m = 0
    n = 0
    for sym in word:
        field[m][n] = sym
        n += 1
        if n == 3:
            n = 0
            m += 1
    return field


def setup(player1, player2):
    syms = ("o", "x")
    #p1, p2 = r.sample([DumbAI(), SmartAI()], 2)
    p1, p2 = (player1(), player2())
    p1.sym, p2.sym = syms
    return p1, p2


def play(p1, p2, field, human = False):
    game_history = []
    field_now = field
    while True:
        for i, p in enumerate((p1, p2)):

            # Make a move
            field_prev = copy.deepcopy(field_now)
            field_now = p._make_move(field_prev)
            print(f"{str(p)} made a move:") if human else None
            show_field(field_now, field_prev) if human else None

            # Check success
            win = check_field(field_now)
            if win is True:
                print(f"{str(p)} ({p.sym}) wins.")
                log = _encode_field(field_now)
                game_history.append(log)
                return (game_history, [p.sym])
            elif win is False:
                print("It's a draw.")
                log = _encode_field(field_now)
                game_history.append(log)
                return (game_history, ["x", "o"])
            elif win is None:
                log = _encode_field(field_now)
                game_history.append(log)
                continue
            else:
                raise Exception("Unhandled case.")


os.system("clear")

# Train the AI
n_games = 100000
training_data = {}
p1, p2 = setup(DumbAI, DumbAI)
c = 0
for _ in tqdm(range(n_games), desc = "Training..."):
    field = [
        [" ", " ", " "], # [0][n]
        [" ", " ", " "], # [1][n]
        [" ", " ", " "], # [2][n]
    ]
    game_data, winner = play(p1, p2, field)
    for turn_state in game_data:
        if turn_state not in training_data:
            training_data[turn_state] = {"x" : 0, "o" : 0}
        for p in winner:
            training_data[turn_state][p] += 1


# Final data processing
for turn_state in training_data:
    for p in training_data[turn_state]:
        training_data[turn_state][p] /= n_games

# Now play the AI
while True:
    os.system("clear")
    pick = [Player, SmartAI]
    r.shuffle(pick)
    p1, p2 = setup(pick[0], pick[1])
    field = [
        # 0    1    2
        [" ", " ", " "], # [0][n]
        [" ", " ", " "], # [1][n]
        [" ", " ", " "], # [2][n]
    ]
    play(p1, p2, field, human = True)
    input("Continue?")

